(ns statikit.core
  (:require [clojure.edn :as edn]
            [clojure.java.io :refer [resource]]
            [statikit.core.env :refer [env]]))

(def ^:private memo-slurp (memoize slurp))

(defn apply-all
  "Takes in a seq of (f arg) pairs,
  returns result of mapping on the application of (f arg)

  If the corresponding arg has an (:apply true)
  pair in it's meta data, then apply the arg to
  f with clojure.core/apply.
  
  ex: (apply-all [[#(+ % 1) 1]] => (2)"
  [fn-arg-pairs]
  (map (fn [[f arg]]
         (if (get (meta arg) :apply)
           (apply f arg)
           (f arg)))
       fn-arg-pairs))

(defn- with-apply-meta
  [obj]
  (with-meta obj {:apply true}))

(defn- slurp-resource
  "Slurps the resource at f, is nil if resource not found"
  [f]
  (some->> (resource f) memo-slurp))

(declare read-arg)

(defn- include
  "Slurps the resource at f, then parses with read-arg"
  ([readers]
   #(include readers %))
  ([readers f]
   (read-arg (slurp-resource f) readers)))

(def default-readers
  {'statikit/apply with-apply-meta
   'statikit/env env
   'statikit/include include
   'statikit/resource slurp-resource
   'statikit/slurp memo-slurp})

(defn- merge-default-readers
  [readers]
  (merge default-readers readers))

(defn read-arg
  "Reads valid edn using clojure.edn/read-string.
  Will use the default statikit readers, as well as any passed in readers"
  ([arg-str]
   (read-arg arg-str {}))
  ([arg-str readers]
   (edn/read-string {:readers (merge-default-readers readers) :eof nil} arg-str)))
