# statikit.core

Provides core functionality for statikit, including:
- apply-all
- read-arg, with readers:
	- #statikit/apply
	- #statikit/env
	- #statikit/include
	- #statikit/slurp

