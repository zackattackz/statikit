(ns statikit.core-test
  (:require [clojure.test :refer :all]
            [statikit.core :refer :all]))

(deftest apply-all-empty
  (testing "apply-all: empty"
    (is (= (apply-all {}) '()))))

(deftest apply-all-one-id
  (testing "apply-all: one (identity)"
    (is (=
         (apply-all {identity 1})
         '(1)))))

(deftest apply-all-one-str
  (testing "apply-all: one (str)"
    (is (=
         (apply-all {str 1})
         '("1")))))

(deftest apply-all-two-id-str
  (testing "apply-all: two (id/str)"
    (is (=
         (apply-all {identity 1 str 2})
         '(1 "2")))))

(deftest apply-all-max-with-apply-meta
  (testing "apply-all: :apply in meta of arg"
    (is (=
         (apply-all {max (with-meta [1 2] {:apply true})})
         '(2)))))

(deftest apply-all-max-without-apply-meta
  (testing "apply-all: :apply not in meta of arg"
    (is (=
         (apply-all {max [1 2]})
         '([1 2])))))


(deftest read-arg-empty
  (testing "read-arg: empty input returns nil"
    (is (=
         (read-arg "")
         nil))))

(deftest read-arg-resource-exists
  (testing "read-arg: #statikit/resource can include an existing resource"
    (is (=
         (read-arg "{:a #statikit/resource \"test.txt\"}")
         {:a "test\n"}))))

(deftest read-arg-resource-not-exists
  (testing "read-arg: #statikit/resource returns nil if not exists"
    (is (=
         (read-arg "{:a #statikit/resource \"notexist.txt\"}")
         {:a nil}))))
