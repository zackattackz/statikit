(defproject org.clojars.zackattackz/lein-statikit "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :scm {:dir ".."}
  :url "https://codeberg.org/zackattackz/statikit"
  :dependencies [[org.clojars.zackattackz/statikit "0.2.0"]]
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :repl-options {:init-ns leiningen.statikit}
  :eval-in-leiningen true)
