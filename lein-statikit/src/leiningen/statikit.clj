(ns leiningen.statikit
  (:require [statikit.core :refer [apply-all]]
            [leiningen.statikit.resources :refer [edn-map]]
            [clojure.string :as s]))

(defn- vec->symbol
  [v]
  (if (< (count v) 2)
    (symbol (apply str v))
    (symbol
      (s/join "." (butlast v))
      (last v))))

(defn- resolve-path
  "Resolves the symbol represented by project-name.path
  path is a string representing a file path, separated by /"
  [project-name path]
  (let [full-path (str project-name "/" path)
        split-path (s/split full-path #"/")
        path-symbol (vec->symbol split-path)]
     (resolve path-symbol)))


(defn statikit
  "I don't do a lot."
  [{:keys [name]} & args]
  (load "testing/two/test.html")
  (println ((resolve 'testing.two/test.html) 1))
  (->> (edn-map)
       (reduce-kv
         (fn [m path arg]
           (assoc m path [(resolve-path name path) arg]))
         {})
       ))
