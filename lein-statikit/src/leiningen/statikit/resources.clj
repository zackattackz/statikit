(ns leiningen.statikit.resources
  (:require [statikit.core :refer [read-arg]]))

(comment "Matches the base of a filename")
(def base-re #"(.*)\.[^\.]*$")

(comment "Matches the extension of a filename")
(def ext-re #"\.[^\.]*$")

(defn- edn-ext?
  [s]
  (->> s
       (re-seq ext-re)
       (first)
       (= ".edn")))

(def base-path "resources")

(defn- edn-paths
  ([]
   (edn-paths base-path))
  ([s]
   (filter
     #(edn-ext? %)
     (map str (file-seq (clojure.java.io/file s))))))

(defn- base-name
  "Returns the base name of a path string

  ex: test         -> test
      test/one     -> one
      test/one/two -> one/two"
  [s]
  (let [v (clojure.string/split s #"/")
        res-vec (if (< (count v) 2)
                       v
                       (subvec v 1))]
         (clojure.string/join "/" res-vec)))

(defn edn-map
  ([]
   (edn-map {:base-path base-path}))
  ([{:keys [base-path readers]}]
   (let [paths (edn-paths base-path)
         read-arg (if readers
                    #(read-arg % readers)
                    #(read-arg %))]
     (zipmap
       (map #(->> % (re-find base-re) (second) (base-name)) paths)
       (map #(->> % (slurp) (read-arg)) paths)))))
